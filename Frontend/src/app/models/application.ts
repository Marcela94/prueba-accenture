export class Application {

    constructor(user = '', company = '', nit = '', salary = 0, dateAdmission = '') {
        this.user = user;
        this.company = company;
        this.nit = nit;
        this.salary = salary;
        this.dateAdmission = dateAdmission;
    }

    user: string;
    company: string;
    nit: string;
    salary: number;
    dateAdmission: string;
}
