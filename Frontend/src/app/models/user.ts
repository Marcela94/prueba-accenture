export class User {

    constructor(identification = '', name = '', lastname = '', birthdate = '') {
        this.identification = identification;
        this.name = name;
        this.lastname = lastname;
        this.birthdate = birthdate;
    }

    identification: string;
    name: string;
    lastname: string;
    birthdate: string;
}
