import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user';
import { NgForm, FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

declare var M: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  // public identification: string;
  // public name: string;
  // public lastname: string;
  // public birthdate: string;
  public user: User;
  public formGroup: FormGroup;

  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.user = new User();
  }

  ngOnInit() {
    this.validateUser();
    this.buildForm();
  }

  async validateUser() {
    const user = await localStorage.getItem('user');
    if (user) {
      this.router.navigate(['/application']);
    }
  }

  private buildForm() {
    this.formGroup = new FormGroup({
      identification: new FormControl('',
        [Validators.maxLength(15),
        Validators.required]
      ),
      name: new FormControl('', Validators.required),
      lastname: new FormControl('', Validators.required),
      birthdate: new FormControl('', Validators.required)
    });
  }

  // public getError(controlName: string): string {
  //   let error = '';
  //   const control = this.formGroup.get(controlName);
  //   if (control.touched && control.errors != null) {
  //     error = JSON.stringify(control.errors);
  //   }
  //   return error;
  // }

  validateId(e) {
    const key = e.charCode;
    if (key < 48 || key > 57) {
      e.preventDefault();
    }
  }

  validateAge() {
    const birtdate = this.formGroup.value.birthdate.split('-');
    const yearOfBirth = parseInt(birtdate[0], 10);
    const monthOfBirth = parseInt(birtdate[1], 10);
    const dayOfBirth = parseInt(birtdate[2], 10);

    // cogemos los valores actuales
    const dateAct = new Date();
    const yearAct = dateAct.getFullYear();
    const monthAct = dateAct.getMonth() + 1;
    const dayAct = dateAct.getDate();

    // Se calculan los años
    let age = yearAct - yearOfBirth;
    if (age === 18) {
      if (monthAct < monthOfBirth) {
        age--;
      }
      if ((monthOfBirth === monthAct) && (dayAct < dayOfBirth)) {
        age--;
      }
    }
    if (age < 18) {
      // M.toast({ html: 'Updated Successfully' });
      alert('Debes ser mayor de 18 años');
    } else {
      this.validExist();
    }
  }

  validExist() {
    this.userService.getUser('/login', this.formGroup.value.identification).subscribe((resp: any) => {
      console.log(resp);
      if (resp.length === 0) {
        this.saveUser();
      } else {
        alert('El usuario ya se encuentra registrado, por favor, inicie sesión');
        this.router.navigate(['/login']);
      }
    });
  }

  saveUser() {
    this.user.identification = this.formGroup.value.identification;
    this.user.name = this.formGroup.value.name;
    this.user.lastname = this.formGroup.value.lastname;
    this.user.birthdate = this.formGroup.value.birthdate;

    this.userService.createUser(this.user, '/register').subscribe(resp => {
      console.log(resp);
      this.router.navigate(['/login']);
    });
  }
}
