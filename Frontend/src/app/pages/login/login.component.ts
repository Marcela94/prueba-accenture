import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public identification: string;

  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.validateUser();
  }

  async validateUser() {
    const user = await localStorage.getItem('user');
    if (user) {
      this.router.navigate(['/application']);
    }
  }

  validateId(e) {
    const key = e.charCode;

    if (key < 48 || key > 57) {
      e.preventDefault();
    }
  }

  submit() {
    this.userService.getUser('/login', this.identification).subscribe((resp: any) => {
      console.log(resp);
      if (resp.length !== 0) {
        localStorage.setItem('user', this.identification);

        this.router.navigate(['/application']);
      } else {
        alert('Cuenta no encontrada, por favor registrate');
      }
    });
  }
}
