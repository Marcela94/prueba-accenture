import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { Application } from '../../models/application';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.scss']
})
export class ApplicationComponent implements OnInit {

  public showForm: boolean;
  public application: Application;
  public formGroup: FormGroup;
  public response: any;

  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.application = new Application();
    this.showForm = true;
  }

  ngOnInit() {
    this.buildForm();
  }

  private buildForm() {
    this.formGroup = new FormGroup({
      nameCompany: new FormControl('', [
        Validators.maxLength(20),
        Validators.required
      ]),
      nitCompany: new FormControl('', Validators.required),
      salary: new FormControl('', [
        Validators.required,
        Validators.maxLength(9)
      ]),
      dateAdmission: new FormControl('', Validators.required)
    });
  }

  validateId(e) {
    const key = e.charCode;

    if (key < 48 || key > 57) {
      e.preventDefault();
    }
  }

  validateDate(): boolean {
    console.log(this.formGroup.value.dateAdmission);

    const date = this.formGroup.value.dateAdmission.split('-');
    const year = parseInt(date[0], 10);
    const month = parseInt(date[1], 10);
    const day = parseInt(date[2], 10);

    // cogemos los valores actuales
    const dateAct = new Date();
    const yearAct = dateAct.getFullYear();
    const monthAct = dateAct.getMonth() + 1;
    const dayAct = dateAct.getDate();


    if (year > yearAct) {
      return false;
    } else {
      if ((year === yearAct) && (month > monthAct)) {
        return false;
      } else if ((month === monthAct) && (day > dayAct)) {
        return false;
      }
      return true;
    }
  }

  submit() {
    this.application.user = localStorage.getItem('user');
    this.application.company = this.formGroup.value.nameCompany;
    this.application.nit = this.formGroup.value.nitCompany;
    this.application.salary = this.formGroup.value.salary;
    this.application.dateAdmission = this.formGroup.value.dateAdmission;

    if (this.validateDate()) {
      this.userService.sendApplication(this.application, '/application').subscribe((resp: any) => {
        console.log(resp);
        this.showForm = false;
        this.response = resp;
      });
    } else {
      alert('La fecha de ingreso no puede ser mayor a la fecha actual');
    }
  }

  formClean() {
    this.showForm = true;
    this.formGroup.reset();
  }

  logout() {
    localStorage.removeItem('user');
    this.router.navigate(['/']);
  }
}
