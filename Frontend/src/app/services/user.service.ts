import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Config } from '../commons/config';
import { User } from '../models/user';
import { Application } from '../models/application';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public url = Config.url;

  constructor(private http: HttpClient) { }

  createUser(user: User, path: string) {
    return this.http.post(this.url.concat(path), user);
  }

  getUser(path: string, id: string) {
    return this.http.get(this.url + `${path}` + `/${id}`);
  }

  sendApplication(req: Application, path: string) {
    return this.http.post(this.url.concat(path), req);
  }

  isLogged(): boolean {
    const user = localStorage.getItem('user');

    if (user) {
      return true;
    }
    return false;
  }
}
