const User = require('../models/user');
const Application = require('../models/application');

const userController = {};

userController.getUsers = async (req, res) => {
    const users = await User.find();
    console.log(users);

    res.json(users);
}

userController.getUser = async (req, res) => {
    const { id } = req.params;
    const user = await User.find({ identification: id });
    res.json(user);
}

userController.createUser = async (req, res, next) => {
    // const user = new User(req.body);
    console.log(req.body);

    const user = new User({
        identification: req.body.identification,
        name: req.body.name,
        lastname: req.body.lastname,
        birthdate: req.body.birthdate
    });

    await user.save();
    res.json({
        status: 'User created'
    });
}

userController.saveApplication = async (req, res) => {
    const application = await new Application(req.body);
    const response = userController.validateApplication(application);
    
    await application.save();
    res.json(response);
}

userController.validateApplication = (info) => {
    console.log(info);

    const date = info.dateAdmission.split('-');
    const year = parseInt(date[0], 10);
    const month = parseInt(date[1], 10);

    // cogemos los valores actuales
    const dateAct = new Date();
    const yearAct = dateAct.getFullYear();
    const monthAct = dateAct.getMonth() + 1;

    let monthsWorked = (yearAct - year) * 12;
    monthsWorked += (monthAct - month);

    console.log(monthsWorked );

    let response = {};

    if (monthsWorked <= 18) {
        response.statusReq = 'rechazado';
        response.quantity = 0;
        response.message = 'Lo sentimos, debes tener mas de 18 meses laborando en ' + info.company + ' para solicitar un credito';
        return response;
    } else {
        if (info.salary < 800000) {
            response.statusReq = 'rechazado';
            response.quantity = 0;
            response.message = 'Lo sentimos, debes tener un salario superior a $800.000 para solicitar un credito';
            return response;
        } else {
            if (info.salary >= 800000 && info.salary < 1000000) {
                response.statusReq = 'aprobado';
                response.quantity = 5000000;
                response.message = 'Felicidades, tu crédito fue aprobado';
                return response;
            } if (info.salary >= 1000000 && info.salary < 4000000) {
                response.statusReq = 'aprobado';
                response.quantity = 20000000;
                response.message = 'Felicidades, tu crédito fue aprobado';
                return response;
            } if (info.salary >= 4000000) {
                response.statusReq = 'aprobado';
                response.quantity = 50000000;
                response.message = 'Felicidades, tu crédito fue aprobado';
                return response;
            }
        }
    }
}

module.exports = userController;