const mongoose = require('mongoose');
const { Schema } = mongoose;

const ApplicationSchema = new Schema({
    user: {type:String, required: true},
    company: { type: String, required: true },
    nit: { type: String, required: true },
    salary: { type: Number, required: true },
    dateAdmission: { type: String, required: true },
});

module.exports = mongoose.model('Application', ApplicationSchema)