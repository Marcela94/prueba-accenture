const mongoose = require('mongoose');
const { Schema } = mongoose;

const UserSchema = new Schema({
    identification: { type: String, required: true },
    name: { type: String, required: true },
    lastname: { type: String, required: true },
    birthdate: { type: String, required: true },
});

module.exports = mongoose.model('User', UserSchema)