const mongoose = require('mongoose');
// const URI = "mongodb://localhost:27017/test";
const URI = 'mongodb+srv://admin:1234@cluster0-2inyt.azure.mongodb.net/test?retryWrites=true&w=majority';

mongoose.connect(URI)
    .then(db => console.log('db is connected'))
    .catch(err => console.error(err));

module.exports = mongoose;