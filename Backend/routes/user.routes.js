const express = require('express');
const router = express.Router();
const userCtrl = require('../controllers/user.controller');

router.get('/', userCtrl.getUsers);
router.get('/login/:id', userCtrl.getUser);
router.post('/register', userCtrl.createUser);
router.post('/application', userCtrl.saveApplication);


module.exports = router;